import './Hero.css'
import img from './hero.jpg'

export default function Hero() {
    return (
        <header>
            <img src={img} className="hero-img" />
        </header>
    )
}