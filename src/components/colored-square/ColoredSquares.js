import cat2 from "./pexels-dominika-roseclay-2686914.jpg";
import cat1 from "./pexels-evg-kowalievska-1170986.jpg";
import "./ColoredSquare.css"

function ColoredSquares(){
    return(
        <div className="square-container">
            <div className="image-wrap-container">
            <div className="colored-square"></div>
                <img className="large-image" src={cat1} alt="cat1." />
                    <div className="sm-img-wrap">
                    <img className="small-image" src={cat2} alt="cat2" />
                    </div>
            </div>
        </div>
    )
}

export default ColoredSquares;
