import logo from './logo.svg';
import ColoredSquares from './components/colored-square/ColoredSquares';
import './App.css';
import Hero from './components/hero/Hero'

function App() {
  return (
    <div>
      <nav>This is the navbar</nav>
      <main>
        <Hero />
        <ColoredSquares />
      </main>
      <footer>Footer goes here</footer>
    </div>
  );
}

export default App;
